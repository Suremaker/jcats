jcats
===========

A java console application allowing to display and download cats images, as well as providing facts about them.

#### Usage

```$ jcats.bat [browser | file | fact | offline]```

where:

* ```browser``` - displays a cat image in browser window,
* ```file``` - downloads a cat image to a desktop directory,
* ```fact``` - prints a fact about cats,
* ```offline``` - opens a random image file, located in desktop directory (of course everybody have only cat images on their desktops ;-) ),

No parameter is equivalent to ```browser``` parameter.


#### How to build and run project

To compile project, please execute ```package.bat``` or ```mvn package``` command.

To run application, please execute ```jcats.bat``` command (project has to be packaged before calling it though).


#### Project dependencies

In order to compile project, a following dependencies are required:

* [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html),
* [Apache Maven](https://maven.apache.org/download.cgi) (recommended 3.3.3 or higher).

The required maven version should be accessible via PATH environment variable.

In order to run project, a [Java 8 JRE](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) is required.

Jcats has been build on Windows OS (it was not tested on other OS types).