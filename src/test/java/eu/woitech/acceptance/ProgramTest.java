package eu.woitech.acceptance;

import com.google.inject.Module;
import eu.woitech.utils.DesktopPathProvider;
import eu.woitech.Program;
import eu.woitech.helpers.AssertThrows;
import eu.woitech.helpers.MockApi;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.*;
import java.net.URI;
import java.util.Properties;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class ProgramTest {
    private final int mockApiPort;
    private MockApi api;

    public ProgramTest() throws IOException {
        mockApiPort = loadMockApiPort();
    }

    @Before
    public void setUp() {
        api = new MockApi(mockApiPort);
    }

    @After
    public void afterTest() throws Exception {
        api.close();
    }

    @Test
    public void calling_program_with_multiple_parameters_should_result_with_help_being_printed_on_console() {
        String output = captureStdOut(() -> Program.main("browser", "file"));
        Assert.assertThat(output, equalTo(String.format("Usage:%njcats [browser | file | fact | offline]%n")));
    }

    @Test
    public void calling_program_with_unrecognized_parameter_should_result_with_help_being_printed_on_console() {
        String output = captureStdOut(() -> Program.main("unrecognized"));
        Assert.assertThat(output, equalTo(String.format("Usage:%njcats [browser | file | fact | offline]%n")));
    }

    @Test
    public void calling_program_with_fact_parameter_should_result_with_fact_being_printed_on_console() {
        String expectedFact = "test_fact";

        api.mockFactApi(expectedFact);
        String fact = captureStdOut(() -> Program.main("fact"));
        Assert.assertThat(fact.trim(), equalTo(expectedFact));
    }

    @Test
    public void calling_program_with_browse_parameter_should_result_with_browser_window_being_opened_with_cat_image() throws IOException {

        Desktop desktop = Mockito.mock(Desktop.class);
        Module override = binder -> binder.bind(Desktop.class).toInstance(desktop);

        Program.create(override).run("browser");

        Mockito.verify(desktop).browse(Mockito.any(URI.class));
    }

    @Test
    public void calling_program_without_parameters_should_result_with_browser_window_being_opened_with_cat_image() throws IOException {

        Desktop desktop = Mockito.mock(Desktop.class);
        Module override = binder -> binder.bind(Desktop.class).toInstance(desktop);

        Program.create(override).run();

        Mockito.verify(desktop).browse(Mockito.any(URI.class));
    }

    @Test
    public void calling_program_with_file_parameter_should_result_with_file_being_downloaded_to_the_desktop() throws IOException {

        String fileName = UUID.randomUUID().toString() + ".txt";
        String fileURL = "/files/" + fileName;
        String fileContent = "some content";

        api.mockCatApi(fileURL);
        api.mockFile(fileURL, fileContent);

        Program.main("file");

        File expectedFilePath = new File(getDesktopPath(), fileName);
        Assert.assertTrue(String.format("File should be downloaded to %s location", expectedFilePath.getPath()), expectedFilePath.exists());
        Assert.assertThat(FileUtils.readFileToString(expectedFilePath), is(equalTo(fileContent)));

        expectedFilePath.delete();
    }

    @Test
    public void calling_program_with_offline_parameter_should_result_with_browser_window_being_opened_image_located_in_desktop_directory() throws IOException {

        File image = createTestDirectoryWithImage();

        Desktop desktop = Mockito.mock(Desktop.class);
        DesktopPathProvider desktopPathProvider = Mockito.mock(DesktopPathProvider.class);
        Mockito.when(desktopPathProvider.getDesktopPath()).thenReturn(image.getParentFile());

        Module override = binder -> {
            binder.bind(Desktop.class).toInstance(desktop);
            binder.bind(DesktopPathProvider.class).toInstance(desktopPathProvider);
        };


        Program.create(override).run("offline");

        Mockito.verify(desktop).browse(image.toURI());

        FileUtils.deleteDirectory(image.getParentFile());
    }

    @Test
    public void a_reason_for_program_failure_should_be_printed_on_console() throws IOException {
        Desktop desktop = Mockito.mock(Desktop.class);
        Mockito.doThrow(new RuntimeException("test")).when(desktop).browse(Mockito.any(URI.class));
        Module override = binder -> binder.bind(Desktop.class).toInstance(desktop);

        String out = captureStdOut(() -> AssertThrows.assertThrows(RuntimeException.class, () -> Program.create(override).run("browser")));

        Assert.assertThat(out, is(String.format("Operation failed:%njava.lang.RuntimeException: test")));

    }

    private File createTestDirectoryWithImage() throws IOException {
        File testDirectory = new File(UUID.randomUUID().toString());
        testDirectory.mkdir();
        File image = new File(testDirectory, "file.png");
        image.createNewFile();
        return image;
    }

    private File getDesktopPath() {
        return FileSystemView.getFileSystemView().getHomeDirectory();
    }

    private static String captureStdOut(Runnable action) {
        ByteArrayOutputStream capturedStream = new ByteArrayOutputStream();
        PrintStream oldOut = System.out;

        try {
            System.setOut(new PrintStream(capturedStream));
            action.run();
            System.out.flush();
        } finally {
            System.setOut(oldOut);
        }

        return capturedStream.toString();
    }

    private int loadMockApiPort() throws IOException {

        try (FileInputStream inputStream = new FileInputStream(getClass().getResource("/config.properties").getFile())) {
            Properties properties = new Properties();
            properties.load(inputStream);
            return Integer.parseInt(properties.getProperty("mockApiPort"));
        }
    }
}