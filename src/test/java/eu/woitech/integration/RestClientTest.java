package eu.woitech.integration;

import eu.woitech.utils.RestClient;
import eu.woitech.helpers.MockApi;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;


public class RestClientTest {
    @Test
    public void getJson_should_retrieve_content() throws Exception {

        try (MockApi api = new MockApi(9797)) {
            TestResource expected = new TestResource("some_text", 5);
            api.mockGetJson("/test", expected);

            TestResource actual = new RestClient().getJson(URI.create("http://localhost:9797/test"), TestResource.class);

            Assert.assertThat("Returned resource is null", actual, is(notNullValue()));
            Assert.assertThat("Returned resource text does not match", actual.getText(), is(equalTo(expected.getText())));
            Assert.assertThat("Returned resource value does not match", actual.getValue(), is(equalTo(expected.getValue())));
        }
    }

    @Test
    public void getXml_should_retrieve_content() throws Exception {

        try (MockApi api = new MockApi(9797)) {
            TestResource expected = new TestResource("some_text", 5);
            api.mockGetXml("/test", expected);

            TestResource actual = new RestClient().getXml(URI.create("http://localhost:9797/test"), TestResource.class);

            Assert.assertThat("Returned resource is null", actual, is(notNullValue()));
            Assert.assertThat("Returned resource text does not match", actual.getText(), is(equalTo(expected.getText())));
            Assert.assertThat("Returned resource value does not match", actual.getValue(), is(equalTo(expected.getValue())));
        }
    }
}

class TestResource {
    private String text;
    private int value;

    TestResource() {
    }

    TestResource(String text, int value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }
}