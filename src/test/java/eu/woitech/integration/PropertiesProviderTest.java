package eu.woitech.integration;

import eu.woitech.settings.PropertiesProvider;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.core.Is.is;

public class PropertiesProviderTest {
    @Test
    public void getFactsUrl_should_return_value_stored_in_property_file() throws IOException {
        Assert.assertThat(new PropertiesProvider().getFactsUrl().toString(), is("http://localhost:9696/mock-facts/"));
    }

    @Test
    public void getCatsUrl_should_return_value_stored_in_property_file() throws IOException {
        Assert.assertThat(new PropertiesProvider().getCatsUrl().toString(), is("http://localhost:9696/mock-cats/"));
    }
}