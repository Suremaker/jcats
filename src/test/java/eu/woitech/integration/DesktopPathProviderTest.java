package eu.woitech.integration;

import eu.woitech.utils.DesktopPathProvider;
import junit.framework.Assert;
import org.junit.Test;

import java.io.File;

public class DesktopPathProviderTest {
    @Test
    public void getDesktopPath_should_return_an_existing_directory() {
        File desktopPath = new DesktopPathProvider().getDesktopPath();
        Assert.assertTrue("Path should refer to existing location", desktopPath.exists());
        Assert.assertTrue("Path should refer to directory", desktopPath.isDirectory());
    }
}