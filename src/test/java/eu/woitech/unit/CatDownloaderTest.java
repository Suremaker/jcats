package eu.woitech.unit;


import eu.woitech.*;
import eu.woitech.entities.CatImage;
import eu.woitech.entities.CatLinksResponse;
import eu.woitech.entities.CatResponseData;
import eu.woitech.helpers.AssertThrows;
import eu.woitech.settings.SettingsProvider;
import eu.woitech.utils.ConsoleWriter;
import eu.woitech.utils.DesktopPathProvider;
import eu.woitech.utils.FileDownloader;
import eu.woitech.utils.RestClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;

import static org.hamcrest.core.Is.is;

public class CatDownloaderTest {
    private URI fullUrl;
    private SettingsProvider settingsProvider;
    private CatDownloader catDownloader;
    private RestClient restClient;
    private DesktopPathProvider desktopPathProvider;
    private FileDownloader fileDownloader;
    private ConsoleWriter console;

    @Before
    public void setUp() throws IOException {
        URI apiBaseUrl = URI.create("http://localhost/");
        fullUrl = apiBaseUrl.resolve("api/images/get?format=xml&results_per_page=1");

        settingsProvider = Mockito.mock(SettingsProvider.class);
        restClient = Mockito.mock(RestClient.class);
        desktopPathProvider = Mockito.mock(DesktopPathProvider.class);
        fileDownloader = Mockito.mock(FileDownloader.class);
        console = Mockito.mock(ConsoleWriter.class);

        Mockito.when(settingsProvider.getCatsUrl()).thenReturn(apiBaseUrl);
        catDownloader = new CatDownloader(settingsProvider, restClient, desktopPathProvider, fileDownloader, console);
    }

    @Test
    public void download_should_download_cat_image_to_desktop_directory() throws IOException {

        String fileName = "some_cat_file.png";
        String desktopDirectory = "c:/home_directory/";
        String catUrl = String.format("http://localhost/some_dir/%s", fileName);
        File targetFilePath = new File(desktopDirectory + fileName);

        Mockito.when(restClient.getXml(fullUrl, CatLinksResponse.class)).thenReturn(new CatLinksResponse(new CatResponseData(new CatImage(catUrl))));
        Mockito.when(desktopPathProvider.getDesktopPath()).thenReturn(new File(desktopDirectory));

        catDownloader.download();

        Mockito.verify(fileDownloader).download(new URL(catUrl), targetFilePath);
        Mockito.verify(console).write("Downloaded file: " + targetFilePath);
    }

    @Test
    public void download_should_throw_IOException_if_no_images_are_accessible() throws IOException {
        Mockito.when(restClient.getXml(fullUrl, CatLinksResponse.class)).thenReturn(new CatLinksResponse(new CatResponseData()));

        IOException ex = AssertThrows.assertThrows(IOException.class, () -> catDownloader.download());
        Assert.assertThat(ex.getMessage(), is("No cat images have been found"));
    }
}