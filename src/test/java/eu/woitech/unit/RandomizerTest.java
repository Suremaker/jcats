package eu.woitech.unit;


import eu.woitech.utils.Randomizer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.IntStream;

import static org.junit.internal.matchers.IsCollectionContaining.hasItems;

public class RandomizerTest {
    @Test
    public void getRandom_should_return_random_element_from_array() {
        Randomizer randomizer = new Randomizer();

        String[] initialArray = new String[]{"text", "other", "another"};
        String[] values = IntStream.range(0, 1000).mapToObj(i -> randomizer.getRandom(initialArray)).distinct().toArray(String[]::new);

        Assert.assertThat("Values should be randomized from initial array", Arrays.asList(initialArray), hasItems(values));
        Assert.assertTrue("Randomizer should not return the same value", values.length > 1);
    }
}