package eu.woitech.unit;

import eu.woitech.utils.DesktopPathProvider;
import eu.woitech.OfflineCatBrowser;
import eu.woitech.utils.Randomizer;
import eu.woitech.helpers.AssertThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.awt.*;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

import static org.hamcrest.core.Is.is;

public class OfflineCatBrowserTest {
    private OfflineCatBrowser offlineCatBrowser;
    private DesktopPathProvider desktopPathProvider;
    private Desktop desktop;
    private File desktopDirectory;
    private Randomizer randomizer;

    @Before
    public void setUp() {
        desktop = Mockito.mock(Desktop.class);
        desktopPathProvider = Mockito.mock(DesktopPathProvider.class);
        desktopDirectory = Mockito.mock(File.class);
        randomizer = Mockito.mock(Randomizer.class);
        Mockito.when(desktopPathProvider.getDesktopPath()).thenReturn(desktopDirectory);
        offlineCatBrowser = new OfflineCatBrowser(desktopPathProvider, desktop, randomizer);
    }

    @Test
    public void browse_should_open_image_located_in_desktop_directory_in_browser() throws IOException {
        File selectedFile = new File("abc.png");
        File[] files = new File[]{selectedFile, new File("def.jpg")};
        Mockito.when(desktopDirectory.listFiles((Mockito.any(FilenameFilter.class)))).thenReturn(files);
        Mockito.when(randomizer.getRandom(files)).thenReturn(selectedFile);

        offlineCatBrowser.browse();

        Mockito.verify(desktop).browse(selectedFile.toURI());
    }

    @Test
    public void browse_should_throw_IOException_if_no_files_found() throws IOException {
        Mockito.when(desktopDirectory.listFiles((Mockito.any(FilenameFilter.class)))).thenReturn(new File[0]);

        IOException exception = AssertThrows.assertThrows(IOException.class, offlineCatBrowser::browse);
        Assert.assertThat(exception.getMessage(), is("No images found in desktop directory"));
    }

    @Test
    public void browse_should_only_select_image_files() throws IOException {

        String[] fileNames = new String[]{"file.txt", "file.PNG", "file.jpg", "file.gif", "file.exe", "file.jar"};
        FilenameFilter filter = captureFileFilter();
        String[] filteredFiles = Arrays.stream(fileNames).filter(name -> filter.accept(null, name)).toArray(String[]::new);

        Assert.assertThat(filteredFiles, is(new String[]{"file.PNG", "file.jpg", "file.gif"}));
    }

    private FilenameFilter captureFileFilter() throws IOException {
        File[] files = new File[]{new File("def.jpg")};
        Mockito.when(desktopDirectory.listFiles((Mockito.any(FilenameFilter.class)))).thenReturn(files);
        Mockito.when(randomizer.getRandom(files)).thenReturn(files[0]);

        offlineCatBrowser.browse();
        ArgumentCaptor<FilenameFilter> argument = ArgumentCaptor.forClass(FilenameFilter.class);
        Mockito.verify(desktopDirectory).listFiles(argument.capture());
        return argument.getValue();
    }
}