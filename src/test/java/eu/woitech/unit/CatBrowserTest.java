package eu.woitech.unit;

import eu.woitech.CatBrowser;
import eu.woitech.settings.SettingsProvider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

public class CatBrowserTest {
    private URI fullUrl;
    private Desktop desktop;
    private SettingsProvider settingsProvider;
    private CatBrowser browser;

    @Before
    public void setUp() throws IOException {
        URI apiBaseUrl = URI.create("http://localhost/");
        fullUrl = apiBaseUrl.resolve("api/images/get?format=src");

        desktop = Mockito.mock(Desktop.class);
        settingsProvider = Mockito.mock(SettingsProvider.class);

        Mockito.when(settingsProvider.getCatsUrl()).thenReturn(apiBaseUrl);
        browser = new CatBrowser(desktop, settingsProvider);
    }

    @Test
    public void browse_should_open_browser_with_cat_image() throws IOException {
        browser.browse();
        Mockito.verify(desktop).browse(fullUrl);
    }
}