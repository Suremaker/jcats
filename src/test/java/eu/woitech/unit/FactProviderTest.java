package eu.woitech.unit;

import eu.woitech.utils.ConsoleWriter;
import eu.woitech.FactProvider;
import eu.woitech.utils.RestClient;
import eu.woitech.settings.SettingsProvider;
import eu.woitech.entities.FactsResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.io.IOException;
import java.net.URI;

import static eu.woitech.helpers.AssertThrows.assertThrows;
import static org.hamcrest.core.Is.is;

public class FactProviderTest {

    private FactProvider provider;
    private RestClient client;
    private SettingsProvider settingsProvider;
    private ConsoleWriter consoleWriter;
    private URI fullUrl;

    @Before
    public void setUp() throws IOException {
        URI apiBaseUrl = URI.create("http://localhost/");
        fullUrl = apiBaseUrl.resolve("/api/facts?number=1");

        client = Mockito.mock(RestClient.class);
        settingsProvider = Mockito.mock(SettingsProvider.class);
        consoleWriter = Mockito.mock(ConsoleWriter.class);

        Mockito.when(settingsProvider.getFactsUrl()).thenReturn(apiBaseUrl);

        provider = new FactProvider(client, settingsProvider, consoleWriter);
    }

    @Test
    public void tellOne_should_print_one_fact() throws IOException {

        String expectedFact = "some_facts";
        Mockito.when(client.getJson(fullUrl, FactsResponse.class)).thenReturn(new FactsResponse(true, expectedFact));
        provider.tellOne();

        Mockito.verify(consoleWriter).write(expectedFact);
    }

    @Test
    public void tellOne_should_throw_IOException_if_result_success_is_false() throws IOException {
        Mockito.when(client.getJson(fullUrl, FactsResponse.class)).thenReturn(new FactsResponse(false, ""));

        IOException exception = assertThrows(IOException.class, () -> provider.tellOne());

        Assert.assertThat(exception.getMessage(), is("Unable to retrieve facts"));
    }

    @Test
    public void tellOne_should_throw_IOException_if_retrieved_facts_are_empty() throws IOException {
        Mockito.when(client.getJson(fullUrl, FactsResponse.class)).thenReturn(new FactsResponse(true));

        IOException exception = assertThrows(IOException.class, () -> provider.tellOne());

        Assert.assertThat(exception.getMessage(), is("Unable to retrieve facts"));
    }

}