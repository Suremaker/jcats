package eu.woitech.helpers;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import eu.woitech.entities.CatImage;
import eu.woitech.entities.CatLinksResponse;
import eu.woitech.entities.CatResponseData;
import eu.woitech.entities.FactsResponse;
import spark.Request;
import spark.Spark;

import java.util.stream.IntStream;


public class MockApi implements AutoCloseable {
    private static XmlMapper xmlMapper = new XmlMapper();
    private final int port;

    static {
        xmlMapper.setVisibility(xmlMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE));
    }

    public MockApi(int port) {
        this.port = port;
        Spark.port(port);
    }

    @Override
    public void close() throws Exception {
        Spark.stop();
    }

    public void mockFactApi(String factToReturn) {
        Spark.get("/mock-facts/api/facts", (request, response) -> new FactsResponse(true, repeatFact(factToReturn, parseFactNumber(request))), new Gson()::toJson);
    }

    private String[] repeatFact(String factToReturn, int factNumber) {
        return IntStream.range(0, factNumber).mapToObj(i -> factToReturn).toArray(String[]::new);
    }

    private int parseFactNumber(Request request) {
        return Integer.parseInt(request.queryParams("number"));
    }

    public <T> void mockGetJson(String path, T model) {
        Spark.get(path, (request, response) -> model, new Gson()::toJson);
    }

    public <T> void mockGetXml(String path, T model) {
        Spark.get(path, (request, response) -> model, xmlMapper::writeValueAsString);
    }

    public void mockCatApi(String fileURL) {
        String fullUrl = String.format("http://localhost:%d%s", port, fileURL);
        Spark.get("/mock-cats/api/images/get", (request, response) -> new CatLinksResponse(new CatResponseData(new CatImage(fullUrl))), xmlMapper::writeValueAsString);
    }

    public void mockFile(String fileURL, String fileContent) {
        Spark.get(fileURL, (request, response) -> fileContent);
    }
}
