package eu.woitech.helpers;

import eu.woitech.utils.Action;
import org.junit.Assert;

public class AssertThrows {

    public static <TException extends Throwable> TException assertThrows(Class<TException> exceptionClass, Action action) {
        try {
            action.run();
        } catch (Throwable e) {
            if (exceptionClass.isInstance(e))
                return (TException) e;

            Assert.fail(String.format("Expected exception of type %s, got %s.", exceptionClass.getName(), e));
        }

        return null;
    }
}
