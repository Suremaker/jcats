package eu.woitech;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Module;
import com.google.inject.util.Modules;
import eu.woitech.settings.PropertiesProvider;
import eu.woitech.settings.SettingsProvider;
import eu.woitech.utils.Action;
import eu.woitech.utils.ConsoleWriter;
import eu.woitech.utils.FileDownloader;
import org.apache.commons.io.FileUtils;

import javax.inject.Inject;
import java.awt.*;
import java.io.PrintStream;

public class Program {

    private final FactProvider factProvider;
    private final CatBrowser catBrowser;
    private final CatDownloader downloader;
    private final OfflineCatBrowser offlineCatBrowser;

    @Inject
    protected Program(FactProvider factProvider, CatBrowser catBrowser, CatDownloader downloader, OfflineCatBrowser offlineCatBrowser) {
        this.factProvider = factProvider;
        this.catBrowser = catBrowser;
        this.downloader = downloader;
        this.offlineCatBrowser = offlineCatBrowser;
    }

    public static Program create(Module... moduleOverrides) {
        return Guice.createInjector(Modules.override(new IoCModule()).with(moduleOverrides))
                .getInstance(Program.class);
    }

    public void run(String... args) {
        try {
            getAction(args).run();
        } catch (Throwable throwable) {
            System.out.printf("Operation failed:%n%s", throwable);
        }
    }

    public static void main(String... args) {
        create().run(args);
    }

    private PrintStream printHelp() {
        return System.out.printf("Usage:%njcats [browser | file | fact | offline]%n");
    }

    private Action getAction(String[] args) {

        if (args == null || args.length > 1)
            return this::printHelp;

        if (args.length == 0)
            return catBrowser::browse;

        switch (args[0]) {
            case "fact":
                return factProvider::tellOne;
            case "browser":
                return catBrowser::browse;
            case "file":
                return downloader::download;
            case "offline":
                return offlineCatBrowser::browse;
            default:
                return this::printHelp;
        }
    }
}

class IoCModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(SettingsProvider.class).to(PropertiesProvider.class);
        binder.bind(ConsoleWriter.class).toInstance(System.out::println);
        binder.bind(Desktop.class).toInstance(Desktop.getDesktop());
        binder.bind(FileDownloader.class).toInstance(FileUtils::copyURLToFile);
    }
}

