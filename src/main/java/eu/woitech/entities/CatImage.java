package eu.woitech.entities;

public class CatImage {
    private String url;

    public CatImage() {
    }

    public CatImage(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
