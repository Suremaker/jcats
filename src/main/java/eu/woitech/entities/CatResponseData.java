package eu.woitech.entities;

import java.util.Arrays;
import java.util.stream.Stream;

public class CatResponseData {
    private CatImage[] images;

    public CatResponseData() {
        this(new CatImage[0]);
    }

    public CatResponseData(CatImage... images) {
        this.images = images;
    }

    public Stream<CatImage> getImages() {
        return Arrays.stream(images);
    }
}
