package eu.woitech.entities;

public class CatLinksResponse {
    private CatResponseData data;

    public CatLinksResponse() {
    }

    public CatLinksResponse(CatResponseData data) {
        this.data = data;
    }

    public CatResponseData getData() {
        return data;
    }
}
