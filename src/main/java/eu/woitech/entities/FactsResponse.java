package eu.woitech.entities;

import java.util.Arrays;
import java.util.stream.Stream;

public class FactsResponse {
    private boolean success;
    private String[] facts;

    public FactsResponse(boolean success, String... facts) {

        this.success = success;
        this.facts = facts;
    }

    public boolean getSuccess() {
        return success;
    }

    public Stream<String> getFacts() {
        return Arrays.stream(facts);
    }
}
