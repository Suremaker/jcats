package eu.woitech;

import com.google.inject.Inject;
import eu.woitech.entities.CatImage;
import eu.woitech.entities.CatLinksResponse;
import eu.woitech.settings.SettingsProvider;
import eu.woitech.utils.ConsoleWriter;
import eu.woitech.utils.DesktopPathProvider;
import eu.woitech.utils.FileDownloader;
import eu.woitech.utils.RestClient;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Optional;

public class CatDownloader {
    private final RestClient client;
    private final DesktopPathProvider desktopPathProvider;
    private final FileDownloader fileDownloader;
    private final ConsoleWriter console;
    private URI catUrl;

    @Inject
    public CatDownloader(SettingsProvider settingsProvider, RestClient client, DesktopPathProvider desktopPathProvider, FileDownloader fileDownloader, ConsoleWriter console) {
        this.client = client;
        this.desktopPathProvider = desktopPathProvider;
        this.fileDownloader = fileDownloader;
        this.console = console;
        this.catUrl = settingsProvider.getCatsUrl().resolve("api/images/get?format=xml&results_per_page=1");
    }

    public void download() throws IOException {
        Optional<CatImage> image = client.getXml(catUrl, CatLinksResponse.class).getData().getImages().findFirst();
        if (!image.isPresent())
            throw new IOException("No cat images have been found");

        File homeDirectory = desktopPathProvider.getDesktopPath();
        URL imageUrl = new URL(image.get().getUrl());
        File target = new File(homeDirectory, Paths.get(imageUrl.getFile()).getFileName().toString());
        fileDownloader.download(imageUrl, target);
        console.write(String.format("Downloaded file: %s", target));
    }

}

