package eu.woitech;

import com.google.inject.Inject;
import eu.woitech.entities.FactsResponse;
import eu.woitech.settings.SettingsProvider;
import eu.woitech.utils.ConsoleWriter;
import eu.woitech.utils.RestClient;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;

public class FactProvider {
    private final URI factUrl;
    private final RestClient client;
    private final ConsoleWriter consoleWriter;

    @Inject
    public FactProvider(RestClient client, SettingsProvider settingsProvider, ConsoleWriter consoleWriter) throws IOException {

        this.client = client;
        this.consoleWriter = consoleWriter;
        this.factUrl = settingsProvider.getFactsUrl().resolve("api/facts?number=1");
    }

    public void tellOne() throws IOException {

        FactsResponse response = client.getJson(factUrl, FactsResponse.class);
        Optional<String> fact = response.getFacts().findFirst();

        if (!response.getSuccess() || !fact.isPresent())
            throw new IOException("Unable to retrieve facts");

        consoleWriter.write(fact.get());
    }

}

