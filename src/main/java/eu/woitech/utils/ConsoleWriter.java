package eu.woitech.utils;

@FunctionalInterface
public interface ConsoleWriter {
    void write(String text);
}
