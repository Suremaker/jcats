package eu.woitech.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URI;

public class RestClient {
    private static final Gson gson = new Gson();
    private static final XmlMapper xmlMapper = new XmlMapper();

    static {
        xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public <T> T getJson(URI uri, Class<T> classOfT) throws IOException {
        return gson.fromJson(getContent(uri), classOfT);
    }

    public <T> T getXml(URI uri, Class<T> classOfT) throws IOException {
        return xmlMapper.readValue(getContent(uri), classOfT);
    }

    private String getContent(URI uri) throws IOException {
        return IOUtils.toString(uri, "UTF-8");
    }
}
