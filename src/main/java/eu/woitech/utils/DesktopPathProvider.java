package eu.woitech.utils;

import javax.swing.filechooser.FileSystemView;
import java.io.File;

public class DesktopPathProvider {

    public File getDesktopPath() {
        return FileSystemView.getFileSystemView().getHomeDirectory();
    }
}
