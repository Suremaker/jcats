package eu.woitech.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

@FunctionalInterface
public interface FileDownloader {
    void download(URL source, File target) throws IOException;
}

