package eu.woitech.utils;

import java.util.Random;

public class Randomizer {
    private final Random random = new Random();

    public <T> T getRandom(T[] elements) {
        return elements[random.nextInt(elements.length)];
    }
}
