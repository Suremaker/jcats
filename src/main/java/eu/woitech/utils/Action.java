package eu.woitech.utils;

@FunctionalInterface
public interface Action {
    void run() throws Throwable;
}
