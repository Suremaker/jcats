package eu.woitech.settings;

import java.net.URI;

public interface SettingsProvider {
    URI getFactsUrl();

    URI getCatsUrl();
}

