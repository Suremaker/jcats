package eu.woitech.settings;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Properties;

public class PropertiesProvider implements SettingsProvider {

    private final Properties properties;

    public PropertiesProvider() throws IOException {
        try (InputStream inputStream = getClass().getResourceAsStream("/config.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);
            this.properties = properties;
        }
    }

    @Override
    public URI getFactsUrl() {
        return URI.create(properties.getProperty("factsUrl"));
    }

    @Override
    public URI getCatsUrl() {
        return URI.create(properties.getProperty("catsUrl"));
    }
}
