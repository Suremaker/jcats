package eu.woitech;

import com.google.inject.Inject;
import eu.woitech.utils.DesktopPathProvider;
import eu.woitech.utils.Randomizer;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

public class OfflineCatBrowser {
    private static final String[] extensions = new String[]{".png", ".gif", ".jpg"};
    private final DesktopPathProvider desktopPathProvider;
    private final Desktop desktop;
    private final Randomizer randomizer;

    @Inject
    public OfflineCatBrowser(DesktopPathProvider desktopPathProvider, Desktop desktop, Randomizer randomizer) {
        this.desktopPathProvider = desktopPathProvider;
        this.desktop = desktop;
        this.randomizer = randomizer;
    }

    public void browse() throws IOException {

        File[] files = desktopPathProvider.getDesktopPath().listFiles((dir, name) -> Arrays.stream(extensions).anyMatch(e -> name.toLowerCase().endsWith(e)));
        if (files.length == 0)
            throw new IOException("No images found in desktop directory");
        desktop.browse(getRandomFileURI(files));
    }

    private URI getRandomFileURI(File[] files) {
        return randomizer.getRandom(files).toURI();
    }
}
