package eu.woitech;

import com.google.inject.Inject;
import eu.woitech.settings.SettingsProvider;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

public class CatBrowser {
    private final URI browseUrl;
    private Desktop desktop;

    @Inject
    public CatBrowser(Desktop desktop, SettingsProvider settingsProvider) {
        this.desktop = desktop;
        this.browseUrl = settingsProvider.getCatsUrl().resolve("api/images/get?format=src");
    }

    public void browse() throws IOException {
        desktop.browse(browseUrl);
    }
}
